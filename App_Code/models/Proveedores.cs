﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Proveedores
/// </summary>
public class Proveedores
{
    public Proveedores(){}
    
    public int id { get; set; }
    public string nombre { get; set; }
    public string direccion { get; set; }
    public int telefono { get; set; }
    public string correo { get; set; }
    public string fecha_create { get; set; }
    public string fecha_update { get; set; }
}