﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Descripción breve de User
/// </summary>
public class User
{
    public User()  { }
        //
        // TODO: Agregar aquí la lógica del constructor
        //
        public int id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public int tipo_usuario { get; set; }
        public int activo { get; set; }
        public string fecha_create { get; set; }


}