﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for WebServiceProveedores
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceProveedores : System.Web.Services.WebService
{

    public WebServiceProveedores()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    public string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
    
    //METODO PARA TRAER TODOS LOS PROVEEDORES
    [WebMethod]
    public void GetAll()
    {
        using (MySqlConnection con = new MySqlConnection(constr))
        {
            using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM proveedores"))
            {
                using (MySqlDataAdapter sda = new MySqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        List<Proveedores> data = new List<Proveedores>();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Proveedores prov = new global::Proveedores();
                            prov.id= Convert.ToInt32(dt.Rows[i]["id"]);
                            prov.nombre = dt.Rows[i]["nombre"].ToString();
                            prov.direccion = dt.Rows[i]["direccion"].ToString();
                            prov.correo = (dt.Rows[i]["correo"].ToString());
                            prov.telefono = Convert.ToInt32(dt.Rows[i]["telefono"].ToString());
                            prov.fecha_create = dt.Rows[i]["fecha_create"].ToString();
                            data.Add(prov);

                        }
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Write(js.Serialize(data));
                    }

                }
            }
        }
    }

    





}
