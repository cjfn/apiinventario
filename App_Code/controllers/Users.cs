﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Web.Script.Serialization;



/// <summary>
/// Descripción breve de WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    public WebService()
    {

        //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
        //InitializeComponent(); 
    }
    public string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

    [WebMethod]
    public string HelloWorld()
    {
        return "Hola a todos";
    }
    //METODO PARA TRAER TODOS LOS USUARIOS
    [WebMethod]
    public void GetAll()
    {
        using (MySqlConnection con = new MySqlConnection(constr))
        {
            using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM users"))
            {
                using (MySqlDataAdapter sda = new MySqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        List<User> data = new List<User>();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            User user = new global::User();
                            user.id = Convert.ToInt32(dt.Rows[i]["id"]);
                            user.email = dt.Rows[i]["email"].ToString();
                            user.password = dt.Rows[i]["password"].ToString();
                            user.tipo_usuario = Convert.ToInt32(dt.Rows[i]["tipo_usuario"].ToString());
                            user.activo = Convert.ToInt32(dt.Rows[i]["activo"].ToString());
                            user.fecha_create = dt.Rows[i]["fecha_create"].ToString();
                            data.Add(user);
                               
                        }
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Write(js.Serialize(data));
                    }
                    
                }
            }
        }
    }


    //METODO PARA TRAER TODOS LOS USUARIOS
    [WebMethod]
    public void GetById(int id)
    {
        using (MySqlConnection con = new MySqlConnection(constr))
        {
            using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM users where id="+id))
            {
                using (MySqlDataAdapter sda = new MySqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        List<User> data = new List<User>();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            User user = new global::User();
                            user.id = Convert.ToInt32(dt.Rows[i]["id"]);
                            user.email = dt.Rows[i]["email"].ToString();
                            user.password = dt.Rows[i]["password"].ToString();
                            user.tipo_usuario = Convert.ToInt32(dt.Rows[i]["tipo_usuario"].ToString());
                            user.activo = Convert.ToInt32(dt.Rows[i]["activo"].ToString());
                            user.fecha_create = dt.Rows[i]["fecha_create"].ToString();
                            data.Add(user);

                        }
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Write(js.Serialize(data));
                    }

                }
            }
        }
    }

    //METODO PARA LOGIN DE USUARIOS
    [WebMethod]
    public void GetLogin(string email, string password)
    {
        using (MySqlConnection con = new MySqlConnection(constr))
        {
            using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM users where email='" + email + "' and password='" + password+"'"))
            {
                using (MySqlDataAdapter sda = new MySqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        int rows = dt.Rows.Count;

                        List<User> data = new List<User>();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        if (rows==0)
                        {
                            string responsefail="No existen registros";
                            Context.Response.Write(js.Serialize(responsefail));

                        }
                        else
                        {

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                User user = new global::User();
                                user.id = Convert.ToInt32(dt.Rows[i]["id"]);
                                user.email = dt.Rows[i]["email"].ToString();
                                user.password = dt.Rows[i]["password"].ToString();
                                user.tipo_usuario = Convert.ToInt32(dt.Rows[i]["tipo_usuario"].ToString());
                                user.activo = Convert.ToInt32(dt.Rows[i]["activo"].ToString());
                                user.fecha_create = dt.Rows[i]["fecha_create"].ToString();
                                data.Add(user);

                            }
                            Context.Response.Write(js.Serialize(data));
                        }
                        
                    }
                    
                }
            }
        }
    }


    //METODO PARA CREACION DE USUARIOS
    [WebMethod]
    public void InsertUser(string email, string password, int tipo_usuario)
    {
        using (MySqlConnection con = new MySqlConnection(constr))
            using (MySqlCommand consulta = new MySqlCommand("SELECT * FROM users where email='" + email + "' and password='" + password + "'"))
            {
                using (MySqlDataAdapter sda = new MySqlDataAdapter())
                {
                    consulta.Connection = con;
                    //VERIFICO SI YA EXISTE USUARIO
                    sda.SelectCommand = consulta;
                    using (DataTable dt = new DataTable())
                    {
                                sda.Fill(dt);
                                int rows = dt.Rows.Count;                      
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                if (rows == 0)
                                {
                                using (MySqlCommand cmd = new MySqlCommand("INSERT INTO users(email, password, tipo_usuario) VALUES ('"+ email +"','"+ password +"','"+ tipo_usuario+"')"))
                                    {
                                        cmd.Connection = con;
                                        con.Open();
                                        cmd.ExecuteNonQuery();
                                        con.Close();
                                        string responsetrue = "Ingresado exitosamente";
                                        Context.Response.Write(js.Serialize(responsetrue));
                                    }
                                }
                                else
                                {
                                    string responsefalse = "Usuario ya existe con estos datos";
                                    Context.Response.Write(js.Serialize(responsefalse));
                                }
                    }
                }
            }
        }


    //METODO PARA MODIFICACION DE USUARIOS
    //update users set email='nuevoemail@gmail.com', password='nuevapass', tipo_usuario=2, fecha_update=CURRENT_TIME where id=2
    [WebMethod]
    public void UpdateUser(int id, string email, string password, int tipo_usuario)
    {
        using (MySqlConnection con = new MySqlConnection(constr))

                        using (MySqlCommand cmd = new MySqlCommand("update users set email='" + email + "',  password='" + password + "',  tipo_usuario='" + tipo_usuario + "', fecha_update=CURRENT_TIME where id="+id))
                        {
                            cmd.Connection = con;
                            con.Open();
                            int estado = cmd.ExecuteNonQuery();
                            con.Close();
                            JavaScriptSerializer js = new JavaScriptSerializer();
            if (estado==0)
                            {
                                string responsetrue = "¨modificado exitosamente";
                                Context.Response.Write(js.Serialize(responsetrue));
                            }
                            else
                            {
                                string responsefalse = "Usuario ya existe con estos datos";
                                Context.Response.Write(js.Serialize(responsefalse));
                            }


        }
    }
                    
}



